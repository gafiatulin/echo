FROM java:jre-alpine
COPY target/echo.jar /app/echo.jar
ENTRYPOINT ["java", "-jar", "\/app\/echo.jar"]