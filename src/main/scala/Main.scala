import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpResponse
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink

import scala.concurrent.duration.FiniteDuration

/**
  * Created by victor on 02/06/16.
  */
object Main extends App {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  val counter = system.actorOf(Props[Counter])
  val defaultDuration = FiniteDuration(60, TimeUnit.SECONDS)

  system.scheduler.schedule(defaultDuration, defaultDuration, counter, Show)(system.dispatcher)

  Http().bind(interface = "0.0.0.0", port = 8000) to Sink.foreach{req =>
    counter ! Inc
    req handleWithSyncHandler  ( _ => HttpResponse())
  } run
}
