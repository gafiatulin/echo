import java.time.temporal.ChronoUnit

import akka.actor.{Actor, ActorLogging}

/**
  * Created by victor on 02/06/16.
  */

sealed trait ActorMessage

case object Inc extends ActorMessage
case object Show extends ActorMessage

class Counter extends Actor with ActorLogging{
  var total = 0L
  var count = 0L

  var last: java.time.ZonedDateTime = java.time.ZonedDateTime.now

  def receive: Receive = {
    case Inc =>
      count += 1
    case Show =>
      val now = java.time.ZonedDateTime.now
      val elapsed = ChronoUnit.SECONDS.between(last, now)
      last = now
      total += count
      log.info("Total: {}, messages since last: {}, throughput: {} m/s", total, count, count / elapsed)
      count = 0
  }
}